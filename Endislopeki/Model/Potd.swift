//
//  Potd.swift
//  Endislopeki
//
//  Created by Viko Wijaya on 29/07/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import Foundation

struct Potd {
    var uid: String
    var image: String
    var title: String
    var description: String
}
