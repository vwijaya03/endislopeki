//
//  Plant.swift
//  Endislopeki
//
//  Created by Viko Wijaya on 15/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import Foundation

public struct Plant: Codable {
    
    public let uid: String
    public let name: String
    public let description: String
    public let recipe: String
    public let img: String
}
