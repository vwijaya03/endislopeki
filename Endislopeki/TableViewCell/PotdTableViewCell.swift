//
//  PotdTableViewCell.swift
//  Endislopeki
//
//  Created by Viko Wijaya on 29/07/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class PotdTableViewCell: UITableViewCell {
    @IBOutlet weak var potdImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization codes
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
