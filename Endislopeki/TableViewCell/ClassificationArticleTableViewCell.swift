//
//  ClassificationArticleTableViewCell.swift
//  FiturNotes
//
//  Created by Kevin Josal on 21/08/19.
//  Copyright © 2019 Kevin Josal. All rights reserved.
//

import UIKit

class ClassificationArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

}
