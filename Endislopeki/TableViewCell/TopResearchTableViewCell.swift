//
//  TopResearchTableViewCell.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 29/07/19.
//  Copyright © 2019 Guildy Harvey. All rights reserved.
//

import UIKit

class TopResearchTableViewCell: UITableViewCell {

    @IBOutlet weak var imageTableView: UIImageView!
    @IBOutlet weak var labelNama: UILabel!
    @IBOutlet weak var labelKeterangan: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
