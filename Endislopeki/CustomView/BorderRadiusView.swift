//
//  BorderRadiusView.swift
//  Endislopeki
//
//  Created by Viko Wijaya on 29/07/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class BorderRadiusView: UIView {
    
}

extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
}

extension BorderRadiusView {
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0
        layer.shadowOffset = .zero
        layer.shadowRadius = 100000
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
