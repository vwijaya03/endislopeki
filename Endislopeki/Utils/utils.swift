//
//  utils.swift
//  Endislopeki
//
//  Created by Viko Wijaya on 25/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import Foundation
import UIKit

public class Utils {
    public lazy var API_HOST = "http://10.62.48.136/endislopeki/public"
    public lazy var API_SEARCH_PLANT = API_HOST + "/search-plant"
    public lazy var API_DISCOVER = API_HOST + "/discover"
    public lazy var API_SAVED_PLANTS = API_HOST + "/saved-plant"
    public lazy var API_FILTER_PLANT = API_HOST + "/filter-plant"
    
    init() {}
    
    func hexStringToUIColor (hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        let formattedCGColor = UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
        
        return formattedCGColor
    }
}
