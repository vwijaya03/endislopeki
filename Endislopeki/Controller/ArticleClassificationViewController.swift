//
//  ArticleClassificationViewController.swift
//  FiturNotes
//
//  Created by Kevin Josal on 19/08/19.
//  Copyright © 2019 Kevin Josal. All rights reserved.
//

import UIKit

class ArticleClassificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var classificationTableView: UITableView!
    
    var dataClassifications: [ [String : Any] ] = []
    
    var titles = [
        "Kingdom",
        "Order",
        "Family",
        "Genus",
        "Kingdom",
        "Order",
        "Family",
        "Genus"
    ]
    var descriptions = [
        "Akar",
        "Plantae",
        "Fittonia",
        "Acathacena",
        "Akar",
        "Plantae",
        "Fittonia",
        "Acathacena"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataClassifications.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = classificationTableView.dequeueReusableCell(withIdentifier: "ClassificationCELL", for: indexPath) as! ClassificationArticleTableViewCell
        cell.titleLabel.text        = dataClassifications[indexPath.row]["classification"] as? String ?? ""
        cell.descriptionLabel.text  = dataClassifications[indexPath.row]["classificationName"] as? String ?? ""
        
        return cell
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //print("Classification View Controller Will Appear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //print("Classification View Controller Will Disappear")
    }

}
