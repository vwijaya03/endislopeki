//
//  ArticleInfoViewController.swift
//  FiturNotes
//
//  Created by Kevin Josal on 19/08/19.
//  Copyright © 2019 Kevin Josal. All rights reserved.
//

import UIKit

class ArticleInfoViewController: UIViewController {
    
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var descriptionString: String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionTextView.text = descriptionString
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //print("Info View Controller Will Disappear")
    }
    

}
