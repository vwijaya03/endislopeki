//
//  ProfilePopUpViewController.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 15/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class ProfilePopUpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func ClosePopUp(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
