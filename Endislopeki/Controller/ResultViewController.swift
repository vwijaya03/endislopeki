//
//  ResultViewController.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 08/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var resultPicture: UIImageView!
    @IBOutlet weak var resultNamePictureLabel: UILabel!
    @IBOutlet weak var resultInfoPictureLabel: UILabel!
    @IBOutlet weak var otherResultCollectionView: UICollectionView!
    @IBOutlet weak var scrollViewView: UIView!    
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    @IBOutlet weak var bgResultView: UIView!
    
    var indexPicture: Int = 0
    
    var varTopResearchImage = [#imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2")]
    var labelNama = ["Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala"]
    var labelKeterangan = ["Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili"]
    
    var otherResultPlantImages = [String]()
    var otherResultPlantNames = [String]()
    var otherResultPlantDescriptions = [String]()
    
    var result: [String: Any] = [:]
    var otherResults: [ [String: Any] ] = [ [:] ]
    
    let utils = Utils.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let plantName = result["name"] as? String ?? ""
        let plantDescription = result["description"] as? String ?? ""
        let plantImg = result["img"] as? String ?? ""
        let completeUrlPlantImg =  utils.API_HOST + plantImg
        
        let url = URL(string: completeUrlPlantImg)!
        let resultData = try? Data(contentsOf: url)
        
        if let resultImageData = resultData {
            result["imgData"] = resultImageData
            resultPicture.image = UIImage(data: resultImageData)
        } else {
            resultPicture.image = varTopResearchImage[indexPicture]
        }
        
        resultNamePictureLabel.text = plantName
        resultInfoPictureLabel.text = plantDescription
        
        bgResultView.backgroundColor = UIColor(patternImage: UIImage(named: "Rectangle")!)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        resultPicture.isUserInteractionEnabled = true
        resultPicture.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toDetailPlantFromResult(_:))))
    }
    
    @objc func toDetailPlantFromResult(_ sender: UITapGestureRecognizer) {
        let articlePhotoSliderVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ArticlePhotoSliderVC") as! ArticlePhotoSliderViewController
        
        articlePhotoSliderVC.dataFromResultPicture = result
        
        self.navigationController?.pushViewController(articlePhotoSliderVC, animated: true)
    }
    
    @objc func toDetailPlantFromOtherResults(_ sender: UITapGestureRecognizer) {
        let articlePhotoSliderVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ArticlePhotoSliderVC") as! ArticlePhotoSliderViewController
        let position = sender.view?.tag ?? 0

        let plantImg = otherResults[position]["img"] as? String ?? ""
        let completeUrlPlantImg =  utils.API_HOST + plantImg
        
        let url = URL(string: completeUrlPlantImg)!
        let resultData = try? Data(contentsOf: url)
        
        if let resultImageData = resultData {
            otherResults[position]["imgData"] = resultImageData
        }
        
        articlePhotoSliderVC.dataFromResultPicture = otherResults[position]

        self.navigationController?.pushViewController(articlePhotoSliderVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        for (_, v) in otherResults.enumerated() {
            let otherPlantImage = v["img"] as? String ?? ""
            let completeUrlOtherPlantImage = utils.API_HOST + otherPlantImage
            let otherPlantName = v["name"] as? String ?? ""
            let otherPlantDescription = v["description"] as? String ?? ""
            
            self.otherResultPlantNames.append(otherPlantName)
            self.otherResultPlantDescriptions.append(otherPlantDescription)
            self.otherResultPlantImages.append(completeUrlOtherPlantImage)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        heightCollectionView.constant = otherResultCollectionView.contentSize.height
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return otherResultPlantNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let otherResultCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "otherResultCollectionCellReuseIdentifier", for:indexPath) as! OtherResultCollectionViewCell
        
            otherResultCollectionViewCell.encyclopediaImage.isUserInteractionEnabled = true
            otherResultCollectionViewCell.encyclopediaImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toDetailPlantFromOtherResults(_:))))
            otherResultCollectionViewCell.encyclopediaImage.tag = indexPath.row
        
            let otherResultUrls = URL(string: otherResultPlantImages[indexPath.row])!
            let otherResultsData = try? Data(contentsOf: otherResultUrls)
        
            if let otherResultImagesData = otherResultsData {
                otherResultCollectionViewCell.encyclopediaImage.image = UIImage(data: otherResultImagesData)
            }
        
            otherResultCollectionViewCell.labelNama.text = otherResultPlantNames[indexPath.row]
            otherResultCollectionViewCell.labelKeterangan.text = otherResultPlantDescriptions[indexPath.row]
        
        return otherResultCollectionViewCell
    }
}
