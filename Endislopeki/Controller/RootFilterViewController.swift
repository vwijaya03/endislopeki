//
//  RootFilterViewController.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 22/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

protocol protocolPassMorphology {
    func passDataMorphology(morphology: String, morphologyTypes: String)
}
class RootFilterViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var rootFilterCollectionView: UICollectionView!
    
    var morphologyTypes = ["Serabut", "Tunggang"]
    var morphologyImages = [#imageLiteral(resourceName: "Akar Serabut"), #imageLiteral(resourceName: "Akar Tunggang")]
    var delegate: protocolPassMorphology? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //let filterStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FilterStoryBoard") as! FilterViewController
        //filterStoryBoard.morphology = "Sistem Perakaran"
        //filterStoryBoard.morphologyType = morphologyTypes[indexPath.row]
        
        self.delegate?.passDataMorphology(morphology: "Sistem Perakaran", morphologyTypes: morphologyTypes[indexPath.row])
        
        dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return morphologyTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let rootFilterCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "rootFilterCollectionCellReuseIdentifier", for:indexPath) as! RootFilterCollectionViewCell
        
        rootFilterCollectionViewCell.filterImage.image = morphologyImages[indexPath.row]
        rootFilterCollectionViewCell.filterLabel.text = morphologyTypes[indexPath.row]
        
        return rootFilterCollectionViewCell
    }
}
