//
//  ResearchViewController.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 26/07/19.
//  Copyright © 2019 Guildy Harvey. All rights reserved.
//

import UIKit

class ResearchViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    @IBOutlet weak var topResearchCollectionView: UICollectionView!
    @IBOutlet weak var topResearchTableView: UITableView!
    @IBOutlet weak var encyclopediaCollectionView: UICollectionView!
    
    //let searchController = UISearchController(searchResultsController: nil)
    
    var varTopResearchImage = [#imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2")]
    var labelNama = ["Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala"]
    var labelKeterangan = ["Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili"]
    
    let utils = Utils.init()
    let searchBar = UISearchBar()
    
    var globalResultsTopResearch: [ [String: Any] ] = [ [:] ]
    var globalResultsMain: [ [String: Any] ] = [ [:] ]
    var globalResultsLatestPlant: [ [String: Any] ] = [ [:] ]
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        searchBar.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpNavBar()
    }
    
    func setUpNavBar() {
        var filterImage    = UIImage(named: "Filter")!
        var profileImage  = UIImage(named: "Profile 3")!
        
        filterImage = filterImage.withRenderingMode(.alwaysOriginal)
        profileImage = profileImage.withRenderingMode(.alwaysOriginal)
        
        let filterButton   = UIBarButtonItem(image: filterImage,  style: .plain, target: self, action: #selector(goToFilter))
        let profileButton = UIBarButtonItem(image: profileImage,  style: .plain, target: self, action: #selector(goToProfile))
        
        searchBar.delegate = self
        searchBar.sizeToFit()
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "Search"
        searchBar.tintColor = UIColor.black
        searchBar.barTintColor = UIColor.gray
        searchBar.isTranslucent = true
        
        navigationItem.titleView = searchBar
        navigationItem.rightBarButtonItems = [profileButton, filterButton]
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let filterStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FilterStoryBoard") as! FilterViewController
        
        self.navigationController?.pushViewController(filterStoryBoard, animated: true)
    }
    
    @objc func goToProfile() {
        let profilePopUp = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfilePopUp") as! ProfilePopUpViewController
        self.addChild(profilePopUp)
        profilePopUp.view.frame = self.view.frame
        self.view.addSubview(profilePopUp.view)
        profilePopUp.didMove(toParent: self)
    }
    
    @objc func goToFilter () {
        let filterStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FilterStoryBoard") as! FilterViewController
        self.navigationController?.pushViewController(filterStoryBoard, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(collectionView == self.topResearchCollectionView) {
            return globalResultsTopResearch.count
        } else {
            return globalResultsMain.count
        }
    }
    
    @objc func toDetailPlant(_ sender: UITapGestureRecognizer) {
        let articlePhotoSliderVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ArticlePhotoSliderVC") as! ArticlePhotoSliderViewController
        let position = sender.view?.tag ?? 0
        let plantImg = globalResultsTopResearch[position]["img"] as? String ?? ""
        let completeUrlPlantImg =  utils.API_HOST + plantImg
        
        let url = URL(string: completeUrlPlantImg)!
        let resultData = try? Data(contentsOf: url)
        
        if let resultImageData = resultData {
            globalResultsTopResearch[position]["imgData"] = resultImageData
        }
        
        articlePhotoSliderVC.dataFromResultPicture = globalResultsTopResearch[position]

        self.navigationController?.pushViewController(articlePhotoSliderVC, animated: true)
    }
    
    @objc func toDetailPlantFromMain(_ sender: UITapGestureRecognizer) {
        let articlePhotoSliderVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ArticlePhotoSliderVC") as! ArticlePhotoSliderViewController
        let position = sender.view?.tag ?? 0
        let plantImg = globalResultsMain[position]["img"] as? String ?? ""
        let completeUrlPlantImg =  utils.API_HOST + plantImg
        
        let url = URL(string: completeUrlPlantImg)!
        let resultData = try? Data(contentsOf: url)
        
        if let resultImageData = resultData {
            globalResultsMain[position]["imgData"] = resultImageData
        }
        
        articlePhotoSliderVC.dataFromResultPicture = globalResultsMain[position]
        
        self.navigationController?.pushViewController(articlePhotoSliderVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == self.topResearchCollectionView) {
            let researchCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "researchCollectionCellReuseIdentifier", for:indexPath) as! TopResearchCollectionViewCell
            
            researchCollectionCell.topResearchImage.isUserInteractionEnabled = true
            researchCollectionCell.topResearchImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toDetailPlant(_:))))
            researchCollectionCell.topResearchImage.tag = indexPath.row
            
            if(globalResultsTopResearch[indexPath.row]["imgData"] != nil) {
                researchCollectionCell.topResearchImage.image = UIImage(data: globalResultsTopResearch[indexPath.row]["imgData"] as! Data)
            }
            //else {
            //    researchCollectionCell.topResearchImage.image = varTopResearchImage[indexPath.row]
            //}
        
            researchCollectionCell.topResearchName.text = globalResultsTopResearch[indexPath.row]["name"] as? String ?? ""
            researchCollectionCell.topResearchDescription.text = globalResultsTopResearch[indexPath.row]["description"] as? String ?? ""
            researchCollectionCell.topResearchDarkBg.backgroundColor = UIColor(patternImage: UIImage(named: "Rectangle")!)
            
            return researchCollectionCell
        } else {
            let encyclopediaCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "encyclopediaCollectionCellReuseIdentifier", for:indexPath) as! EncyclopediaCollectionViewCell
            
            encyclopediaCollectionCell.encyclopediaImage.isUserInteractionEnabled = true
            encyclopediaCollectionCell.encyclopediaImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toDetailPlantFromMain(_:))))
            encyclopediaCollectionCell.encyclopediaImage.tag = indexPath.row
            
            if(globalResultsMain[indexPath.row]["imgData"] != nil) {
                encyclopediaCollectionCell.encyclopediaImage.image = UIImage(data: globalResultsMain[indexPath.row]["imgData"] as! Data)
            }
            
            encyclopediaCollectionCell.labelNama.text = globalResultsMain[indexPath.row]["name"] as? String ?? ""
            encyclopediaCollectionCell.labelKeterangan.text = globalResultsMain[indexPath.row]["description"] as? String ?? ""
            
            return encyclopediaCollectionCell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return globalResultsLatestPlant.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let researchTableCell = tableView.dequeueReusableCell(withIdentifier: "researchTableCellReuseIdentifier", for:indexPath) as! TopResearchTableViewCell
        
        if(globalResultsTopResearch[indexPath.row]["imgData"] != nil) {
            researchTableCell.imageTableView.image = UIImage(data: globalResultsLatestPlant[indexPath.row]["imgData"] as! Data)
        }
        
        researchTableCell.labelNama.text = globalResultsLatestPlant[indexPath.row]["name"] as? String ?? ""
        researchTableCell.labelKeterangan.text = globalResultsLatestPlant[indexPath.row]["description"] as? String ?? ""
        
        return researchTableCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func getData() {
        let urlSession = URLSession.shared
        guard let url = URL(string: self.utils.API_DISCOVER) else {
            return
        }
        
        guard let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            return
        }
        
        var urlRequest = URLRequest(url: urlComponents.url!)
        urlRequest.httpMethod = "GET"
        
        let task = urlSession.dataTask(with: urlRequest) { data, response, error in
            if error != nil || data == nil {
                print("Client error!")
                return
            }
            
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error!")
                return
            }
            
            guard let mime = response.mimeType, mime == "application/json" else {
                print("Wrong MIME type!")
                return
            }
            
            do {
                let response = try JSONSerialization.jsonObject(with: data!, options: [])
                let generalData = response as? [ String: Any ]
                var resultsTopResearch = generalData?["resultTopResearch"] as? [ [String: Any] ] ?? [ [:] ]
                var resultsMain = generalData?["resultMain"] as? [ [String: Any] ] ?? [ [:] ]
                var resultsLatestPlant = generalData?["resultLatestPlant"] as? [ [String: Any] ] ?? [ [:] ]
                
                for (i, _) in resultsTopResearch.enumerated() {
                    let topResearchPlantImg = resultsTopResearch[i]["img"] as? String ?? ""
                    let completeTopResearchPlantImgUrl = self.utils.API_HOST + topResearchPlantImg
                    
                    let topResearchImgUrls = URL(string: completeTopResearchPlantImgUrl)!
                    let topResearchImgData = try? Data(contentsOf: topResearchImgUrls)
                    
                    if let topResearchImg = topResearchImgData {
                        resultsTopResearch[i]["imgData"] = topResearchImg
                    }
                }
                
                for (i, _) in resultsMain.enumerated() {
                    let mainPlantImg = resultsMain[i]["img"] as? String ?? ""
                    let completeMainPlantImgUrl = self.utils.API_HOST + mainPlantImg
                    
                    let mainResultImgUrls = URL(string: completeMainPlantImgUrl)!
                    let mainResultImgData = try? Data(contentsOf: mainResultImgUrls)
                    
                    if let mainResultImg = mainResultImgData {
                        resultsMain[i]["imgData"] = mainResultImg
                    }
                }
                
                for (i, _) in resultsLatestPlant.enumerated() {
                    let latestPlantImg = resultsTopResearch[i]["img"] as? String ?? ""
                    let completeLatestPlantImgUrl = self.utils.API_HOST + latestPlantImg
                    
                    let latestImgUrls = URL(string: completeLatestPlantImgUrl)!
                    let latestImgData = try? Data(contentsOf: latestImgUrls)
                    
                    if let latestImg = latestImgData {
                        resultsLatestPlant[i]["imgData"] = latestImg
                    }
                }
                
                DispatchQueue.main.async() {
                    self.globalResultsTopResearch = resultsTopResearch
                    self.globalResultsMain = resultsMain
                    self.globalResultsLatestPlant = resultsLatestPlant
                    
                    self.encyclopediaCollectionView.reloadData()
                    self.topResearchCollectionView.reloadData()
                    self.topResearchTableView.reloadData()
                }
            } catch {
                print("JSON error: \(error.localizedDescription)")
            }
        }
        
        task.resume()
    }
}
