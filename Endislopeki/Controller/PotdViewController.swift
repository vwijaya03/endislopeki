//
//  PotdViewController.swift
//  Endislopeki
//
//  Created by Viko Wijaya on 29/07/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class PotdViewController: UIViewController {
    @IBOutlet weak var backButton: UIButton!
    
    var potds: [Potd] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        potds = [
            Potd(uid: UUID().uuidString, image: "potd1", title: "Putri Malu", description: "Tanaman ini sangat memalukan"),
            Potd(uid: UUID().uuidString, image: "potd2", title: "Kecombrang", description: "Tanaman ini adalah kecombrang"),
            Potd(uid: UUID().uuidString, image: "potd3", title: "Pointsettia", description: "Tanaman ini adalah Pointsettia"),
            Potd(uid: UUID().uuidString, image: "potd4", title: "Alang - Alang", description: "Tanaman ini adalah Alang - Alang"),
            Potd(uid: UUID().uuidString, image: "potd1", title: "Stingy Nettle", description: "Tanaman ini adalah Stingy Nettle"),
            Potd(uid: UUID().uuidString, image: "potd2", title: "Mangga Muda", description: "Tanaman ini adalah Mangga Muda"),
            Potd(uid: UUID().uuidString, image: "potd3", title: "Kelor", description: "Tanaman ini adalah Kelor"),
            Potd(uid: UUID().uuidString, image: "potd1", title: "Putri Malu", description: "Tanaman ini sangat memalukan"),
            Potd(uid: UUID().uuidString, image: "potd2", title: "Kecombrang", description: "Tanaman ini adalah kecombrang"),
            Potd(uid: UUID().uuidString, image: "potd3", title: "Pointsettia", description: "Tanaman ini adalah Pointsettia"),
            Potd(uid: UUID().uuidString, image: "potd4", title: "Alang - Alang", description: "Tanaman ini adalah Alang - Alang"),
            Potd(uid: UUID().uuidString, image: "potd1", title: "Stingy Nettle", description: "Tanaman ini adalah Stingy Nettle"),
            Potd(uid: UUID().uuidString, image: "potd2", title: "Mangga Muda", description: "Tanaman ini adalah Mangga Muda"),
            Potd(uid: UUID().uuidString, image: "potd3", title: "Kelor", description: "Tanaman ini adalah Kelor"),
        ]
        
        backButton.titleLabel?.adjustsFontSizeToFitWidth = true
    }
}

extension PotdViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return potds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PotdTableViewCell", for: indexPath) as! PotdTableViewCell
        let data = potds[indexPath.row]
        
        cell.potdImageView.image = UIImage(named: data.image)
        cell.titleLabel.text = data.title
        cell.descriptionLabel.text = data.description
        
        return cell
    }
}
