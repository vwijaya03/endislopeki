//
//  SavedArticleViewController.swift
//  FiturNotes
//
//  Created by Kevin Josal on 12/08/19.
//  Copyright © 2019 Kevin Josal. All rights reserved.
//

import UIKit
import CoreData

class SavedArticleViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var addNotesButton: UIButton!
    @IBOutlet weak var savedPlantsCollectionView: UICollectionView!
    
    let utils = Utils.init()
    let defaults = UserDefaults.standard
    
    var globalResultsSavedPlant: [ [String: Any] ] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return globalResultsSavedPlant.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "savedArticleCell", for: indexPath as IndexPath) as! SavedArticleCollectionViewCell
        
        cell.savedArticleImage.isUserInteractionEnabled = true
        cell.savedArticleImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toDetailPlant(_:))))
        cell.savedArticleImage.tag = indexPath.row
        
        if(globalResultsSavedPlant[indexPath.row]["imgData"] != nil) {
            cell.savedArticleImage.image = UIImage(data: globalResultsSavedPlant[indexPath.row]["imgData"] as! Data)
        }
        
        cell.savedArticleTitle.text = globalResultsSavedPlant[indexPath.row]["name"] as? String ?? ""
        cell.savedArticleDescription.text = globalResultsSavedPlant[indexPath.row]["description"] as? String ?? ""
        
        return cell
    }
    
    @objc func toDetailPlant(_ sender: UITapGestureRecognizer) {
        let articlePhotoSliderVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ArticlePhotoSliderVC") as! ArticlePhotoSliderViewController
        let position = sender.view?.tag ?? 0
        let plantImg = globalResultsSavedPlant[position]["img"] as? String ?? ""
        let completeUrlPlantImg =  utils.API_HOST + plantImg
        
        let url = URL(string: completeUrlPlantImg)!
        let resultData = try? Data(contentsOf: url)
        
        if let resultImageData = resultData {
            globalResultsSavedPlant[position]["imgData"] = resultImageData
        }
        
        articlePhotoSliderVC.dataFromResultPicture = globalResultsSavedPlant[position]
        
        self.navigationController?.pushViewController(articlePhotoSliderVC, animated: true)
    }

    @IBAction func addNotesButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "openEmptyNotesSegue", sender: nil)
    }
    
    func getData() {
        let savedPlants = defaults.stringArray(forKey: "plants") ?? [String]()
        var savedPlantsStr: String = ""
        
        for (index, value) in savedPlants.enumerated() {
            if(index > -1) {
                if(index == (savedPlants.count - 1)) {
                    savedPlantsStr = savedPlantsStr + value
                } else {
                    savedPlantsStr = savedPlantsStr + value + ", "
                }
                
            }
        }
        
        let urlSession = URLSession.shared
        guard let url = URL(string: self.utils.API_SAVED_PLANTS) else {
            return
        }
        
        guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            return
        }
        
        var items = [URLQueryItem]()
        items.append(URLQueryItem(name: "savedPlants", value: savedPlantsStr))
        urlComponents.queryItems = items
        
        var urlRequest = URLRequest(url: urlComponents.url!)
        urlRequest.httpMethod = "GET"
        
        let task = urlSession.dataTask(with: urlRequest) { data, response, error in
            if error != nil || data == nil {
                print("Client error!")
                return
            }
            
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error!")
                return
            }
            
            guard let mime = response.mimeType, mime == "application/json" else {
                print("Wrong MIME type!")
                return
            }
            
            do {
                let response = try JSONSerialization.jsonObject(with: data!, options: [])
                let generalData = response as? [ String: Any ]
                var resultsSavedPlant = generalData?["result"] as? [ [String: Any] ] ?? []
                
                for (i, _) in resultsSavedPlant.enumerated() {
                    let savedPlantsImg = resultsSavedPlant[i]["img"] as? String ?? ""
                    let completeSavedPlantsImgUrl = self.utils.API_HOST + savedPlantsImg
                    
                    let savedImgUrls = URL(string: completeSavedPlantsImgUrl)!
                    let savedPlantImgData = try? Data(contentsOf: savedImgUrls)
                    
                    if let savedPlantImg = savedPlantImgData {
                        resultsSavedPlant[i]["imgData"] = savedPlantImg
                    }
                }
                
                DispatchQueue.main.async() {
                    self.globalResultsSavedPlant = resultsSavedPlant
                    
                    self.savedPlantsCollectionView.reloadData()
                }
            } catch {
                print("JSON error: \(error.localizedDescription)")
            }
        }
        
        task.resume()

        print(savedPlantsStr)
    }
}
