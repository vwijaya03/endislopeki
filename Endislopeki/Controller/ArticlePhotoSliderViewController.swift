//
//  ArticlePhotoSliderViewController.swift
//  FiturNotes
//
//  Created by Kevin Josal on 12/08/19.
//  Copyright © 2019 Kevin Josal. All rights reserved.
//

import UIKit
import CoreData

class ArticlePhotoSliderViewController: UIViewController, UIScrollViewDelegate{

    @IBOutlet weak var articlePhotoSlider: ArticlePhotoSliderView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var containerViewInfo: UIView!
    @IBOutlet weak var containerViewClassification: UIView!
    @IBOutlet weak var containerViewRecipe: UIView!
    @IBOutlet weak var containerViewMorphology: UIView!
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    let defaults = UserDefaults.standard
    let utils = Utils.init()
    
    var dataFromResultPicture: [String: Any] = [:]
    var images: [UIImage] = []
    var articleArr: [NSManagedObject] = []
    var clickedValue:Int = 0
    var arrSavedPlants = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        segmentedControl.backgroundColor = .clear
        segmentedControl.tintColor = .clear
        
        let attributes: [NSAttributedString.Key : Any] = [.foregroundColor : UIColor.gray]
        segmentedControl.setTitleTextAttributes(attributes, for: .normal)
        
        let selectedAttrib: [NSAttributedString.Key : Any] = [.foregroundColor : utils.hexStringToUIColor(hex: "#57A81C")]
        segmentedControl.setTitleTextAttributes(selectedAttrib, for: .selected)
        
        let imgArticleData = dataFromResultPicture["imgData"] as? Data
        
        if let filteredImageData = imgArticleData {
            images.append(UIImage(data: filteredImageData)!)
        }
        
        articlePhotoSlider.configure(with: images)
        
        titleLabel.text = dataFromResultPicture["name"] as? String ?? ""
        subTitleLabel.text = dataFromResultPicture["subName"] as? String ?? ""
        
        let savedPlants = defaults.stringArray(forKey: "plants") ?? [String]()
        
        if(savedPlants.contains(dataFromResultPicture["UID"] as? String ?? "")) {
            saveButton.isHidden = true
            editButton.isHidden = false
        }
    }
    
    @IBAction func indexChanged(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            changeView(view: 0)
        case 1:
            changeView(view: 1)
        case 2:
            changeView(view: 2)
        case 3:
            changeView(view: 3)
            default:
            break
        }
    }
    
    func changeView (view: Int)
    {
        if(view == 0)
        {
            containerViewInfo.isHidden              = false
            containerViewClassification.isHidden    = true
            containerViewRecipe.isHidden            = true
            containerViewMorphology.isHidden        = true
        }
        else if(view == 1)
        {
            containerViewInfo.isHidden              = true
            containerViewClassification.isHidden    = false
            containerViewRecipe.isHidden            = true
            containerViewMorphology.isHidden        = true
        }
        else if(view == 2)
        {
            containerViewInfo.isHidden              = true
            containerViewClassification.isHidden    = true
            containerViewRecipe.isHidden            = false
            containerViewMorphology.isHidden        = true
        }
        else if(view == 3)
        {
            containerViewInfo.isHidden              = true
            containerViewClassification.isHidden    = true
            containerViewRecipe.isHidden            = true
            containerViewMorphology.isHidden        = false
        }
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        var savedPlants = defaults.stringArray(forKey: "plants") ?? [String]()
        savedPlants.append(dataFromResultPicture["UID"] as? String ?? "")
        //print(dataFromResultPicture["UID"] as? String ?? "")
        defaults.set(savedPlants, forKey: "plants")
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {return}
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let article                 = Article(context: managedContext)
        article.id_article          = self.dataFromResultPicture["UID"] as? String
        
        saveButton.isHidden = true
        editButton.isHidden = false
    }
    
    @IBAction func editButtonPressed(_ sender: Any) {
        
        performSegue(withIdentifier: "notesSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination is ArticleInfoViewController {
            let articleInfoVC = segue.destination as? ArticleInfoViewController
            articleInfoVC?.descriptionString = self.dataFromResultPicture["description"] as? String ?? ""
        } else if segue.destination is ArticleRecipeViewController {
            let articleRecipeVC = segue.destination as? ArticleRecipeViewController
            articleRecipeVC?.recipeDescription = self.dataFromResultPicture["recipe"] as? String ?? ""
        } else if segue.destination is ArticleClassificationViewController {
            let articleClassificationVC = segue.destination as? ArticleClassificationViewController
            articleClassificationVC?.dataClassifications = dataFromResultPicture["classifications"] as? [ [String : Any] ] ?? []
        } else if segue.destination is ArticleMorphologyViewController {
            let articleMorphologyVC = segue.destination as? ArticleMorphologyViewController
            articleMorphologyVC?.dataMorphologies = dataFromResultPicture["morfologies"] as? [ [String : Any] ] ?? []
        } else if segue.identifier == "notesSegue" {
            let destinationNavigationController = segue.destination as! UINavigationController
            let targetController = destinationNavigationController.topViewController as? NotesViewController
            targetController?.articleIDPass = self.dataFromResultPicture["UID"] as? String ?? ""
            targetController?.articleName = self.dataFromResultPicture["name"] as? String ?? ""
            targetController?.articleSubName = self.dataFromResultPicture["subName"] as? String ?? ""
        }
    }
}

