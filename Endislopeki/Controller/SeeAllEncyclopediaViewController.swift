//
//  SeeAllEncyclopediaViewController.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 08/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class SeeAllEncyclopediaViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
   

    @IBOutlet weak var SeeAllEncyclopediaCollectionView: UICollectionView!
    
    var varTopResearchImage = [#imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2")]
    var labelNama = ["Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala"]
    var labelKeterangan = ["Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return varTopResearchImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let seeAllEncyclopediaCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "seeAllEncyclopediaCollectionCellReuseIdentifier", for:indexPath) as! SeeAllEncyclopediaCollectionViewCell
        seeAllEncyclopediaCollectionCell.encyclopediaImage.image = varTopResearchImage[indexPath.row]
        seeAllEncyclopediaCollectionCell.labelNama.text = labelNama[indexPath.row]
        seeAllEncyclopediaCollectionCell.labelKeterangan.text = labelKeterangan[indexPath.row]
        
        return seeAllEncyclopediaCollectionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let resultViewController = storyBoard.instantiateViewController(withIdentifier: "ResultViewController") as! ResultViewController
        print("index path row see all")
        print(indexPath.row)
        resultViewController.indexPicture = indexPath.row
        self.navigationController?.pushViewController(resultViewController, animated: true)
        
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let resultViewController = storyBoard.instantiateViewController(withIdentifier: "ResearchViewController") as! ResearchViewController
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
