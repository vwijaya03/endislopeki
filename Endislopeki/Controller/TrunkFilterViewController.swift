//
//  TrunkFilterViewController.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 23/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class TrunkFilterViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    var morphologyTypes = ["Basah", "Berkayu", "Rumput", "Bersegi", "Bulat", "Pipih", "Dichotomous", "Monopodial", "Simpodial"]
    var morphologyImages = [#imageLiteral(resourceName: "Batang Basah"), #imageLiteral(resourceName: "Batang Berkayu"), #imageLiteral(resourceName: "Batang Rumput"), #imageLiteral(resourceName: "Batang Bersegi"), #imageLiteral(resourceName: "Batang Bulat"), #imageLiteral(resourceName: "Batang Pipih"), #imageLiteral(resourceName: "Dichotomous"), #imageLiteral(resourceName: "Monopodial"), #imageLiteral(resourceName: "Simpodial")]
    
    @IBOutlet weak var trunkFilterCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return morphologyTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let trunkFilterCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "trunkFilterCollectionCellReuseIdentifier", for:indexPath) as! TrunkFilterCollectionViewCell
        
        trunkFilterCollectionViewCell.filterImage.image = morphologyImages[indexPath.row]
        trunkFilterCollectionViewCell.filterLabel.text = morphologyTypes[indexPath.row]
        
        return trunkFilterCollectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let filterStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FilterStoryBoard") as! FilterViewController
        let indexPathNumber = morphologyTypes[indexPath.row]
        print(indexPathNumber)
        dismiss(animated: true, completion: nil)
    }

   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
