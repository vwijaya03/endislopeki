//
//  FilterViewController.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 12/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate, protocolPassMorphology {
    
    let searchBar = UISearchBar()
    let utils = Utils.init()
    
    var varTopResearchImage = [#imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 4"), #imageLiteral(resourceName: "Gambar 3"), #imageLiteral(resourceName: "Gambar 1"), #imageLiteral(resourceName: "Gambar 2")]
    var labelNama = ["Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala", "Putri Malu", "Daun Kelor", "Belalang", "Padi", "Lalalala"]
    var labelKeterangan = ["Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili", "Gampang malu emang orangnya", "The New Superfood", "Harusnya hewan, ha?", "Padang sawah", "Alalalilili"]
    var imageButton = [#imageLiteral(resourceName: "Group 4"), #imageLiteral(resourceName: "Group 5"), #imageLiteral(resourceName: "Group 6"), #imageLiteral(resourceName: "Group 7"), #imageLiteral(resourceName: "Group 8"), #imageLiteral(resourceName: "Group 9")]

    
    @IBOutlet weak var filterButtonCollectionView: UICollectionView!
    @IBOutlet weak var filterCollectionView: UICollectionView!
    
    var searchData: String = ""
    var morphology: String = ""
    var morphologyType: String = ""
    
    var globalResultsFilterPlant: [ [String: Any] ] = [ [:] ]
    
    func passDataMorphology(morphology: String, morphologyTypes: String) {
        self.morphology = morphology
        self.morphologyType = morphologyTypes
        
        getData(search: self.searchData, morphology: self.morphology, morphologyType: self.morphologyType)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filterCollectionView.delegate = self
        
        setUpNavBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getData(search: self.searchData, morphology: self.morphology, morphologyType: self.morphologyType)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    func setUpNavBar() {
        searchBar.delegate = self
        searchBar.sizeToFit()
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "Search"
        searchBar.tintColor = UIColor.black
        searchBar.barTintColor = UIColor.gray
        searchBar.isTranslucent = true
        searchBar.showsCancelButton = true
        searchBar.becomeFirstResponder()
        
        navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchBar.showsCancelButton = false
        
        self.searchData = ""
        self.morphology = ""
        self.morphologyType = ""
        
        getData(search: self.searchData, morphology: self.morphology, morphologyType: self.morphologyType)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchData = searchBar.text ?? ""
        
        getData(search: self.searchData, morphology: self.morphology, morphologyType: self.morphologyType)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == self.filterCollectionView) {
            return globalResultsFilterPlant.count
        }
        else {
            return imageButton.count
        }
    }
    
    @objc func toDetailPlant(_ sender: UITapGestureRecognizer) {
        let articlePhotoSliderVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ArticlePhotoSliderVC") as! ArticlePhotoSliderViewController
        let position = sender.view?.tag ?? 0
        let plantImg = globalResultsFilterPlant[position]["img"] as? String ?? ""
        let completeUrlPlantImg =  utils.API_HOST + plantImg
        
        let url = URL(string: completeUrlPlantImg)!
        let resultData = try? Data(contentsOf: url)
        
        if let resultImageData = resultData {
            globalResultsFilterPlant[position]["imgData"] = resultImageData
        }
        
        articlePhotoSliderVC.dataFromResultPicture = globalResultsFilterPlant[position]
        
        self.navigationController?.pushViewController(articlePhotoSliderVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == self.filterCollectionView) {
            let filterCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterCollectionCellReuseIdentifier", for:indexPath) as! FilterCollectionViewCell
            
            filterCollectionViewCell.filterImage.isUserInteractionEnabled = true
            filterCollectionViewCell.filterImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toDetailPlant(_:))))
            filterCollectionViewCell.filterImage.tag = indexPath.row
            
            if(globalResultsFilterPlant[indexPath.row]["imgData"] != nil) {
                filterCollectionViewCell.filterImage.image = UIImage(data: globalResultsFilterPlant[indexPath.row]["imgData"] as! Data)
            }
            
            filterCollectionViewCell.labelNama.text = globalResultsFilterPlant[indexPath.row]["name"] as? String ?? ""
            filterCollectionViewCell.labelKeterangan.text = globalResultsFilterPlant[indexPath.row]["description"] as? String ?? ""
            
            return filterCollectionViewCell
        }
        else {
            let filterButtonCollectionView = collectionView.dequeueReusableCell(withReuseIdentifier: "filterButtonCollectionView", for: indexPath) as! FilterButtonCollectionViewCell
            
            filterButtonCollectionView.imageButton.image = imageButton[indexPath.row]
            
            return filterButtonCollectionView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.filterCollectionView {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let resultViewController = storyBoard.instantiateViewController(withIdentifier: "ResultViewController") as! ResultViewController
            
            resultViewController.indexPicture = indexPath.row
            self.navigationController?.pushViewController(resultViewController, animated: true)
        }
        else {
            if indexPath.row == 0 {
                print("masuk 0")
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let rootFilterViewController = storyBoard.instantiateViewController(withIdentifier: "RootFilterViewController") as! RootFilterViewController
                
                rootFilterViewController.modalPresentationStyle = .overCurrentContext
                rootFilterViewController.delegate = self
                
                present(rootFilterViewController, animated: true, completion: nil)
            }
            else if indexPath.row == 1 {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let trunkFilterViewController = storyBoard.instantiateViewController(withIdentifier: "TrunkFilterViewController") as! TrunkFilterViewController
                
            trunkFilterViewController.modalPresentationStyle = .overCurrentContext
                present(trunkFilterViewController, animated: true, completion: nil)
            }
            else if indexPath.row == 2 {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let leafFilterViewController = storyBoard.instantiateViewController(withIdentifier: "LeafFilterViewController") as! LeafFilterViewController
                
                leafFilterViewController.modalPresentationStyle = .overCurrentContext
                present(leafFilterViewController, animated: true, completion: nil)
            }
            else if indexPath.row == 3 {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let flowerFilterViewController = storyBoard.instantiateViewController(withIdentifier: "FlowerFilterViewController") as! FlowerFilterViewController
                
                flowerFilterViewController.modalPresentationStyle = .overCurrentContext
                present(flowerFilterViewController, animated: true, completion: nil)
            }
            else if indexPath.row == 4 {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let fruitFilterViewController = storyBoard.instantiateViewController(withIdentifier: "FruitFilterViewController") as! FruitFilterViewController
                
                fruitFilterViewController.modalPresentationStyle = .overCurrentContext
                present(fruitFilterViewController, animated: true, completion: nil)
            }
            else if indexPath.row == 5 {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let seedFilterViewController = storyBoard.instantiateViewController(withIdentifier: "SeedFilterViewController") as! SeedFilterViewController
                
                seedFilterViewController.modalPresentationStyle = .overCurrentContext
                present(seedFilterViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    func getData(search: String, morphology: String, morphologyType: String) {
        let urlSession = URLSession.shared
        guard let url = URL(string: self.utils.API_FILTER_PLANT) else {
            return
        }
        
        guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            return
        }
        
        var items = [URLQueryItem]()
        items.append(URLQueryItem(name: "search", value: search))
        items.append(URLQueryItem(name: "morphology", value: morphology))
        items.append(URLQueryItem(name: "morphologyType", value: morphologyType))
        urlComponents.queryItems = items
        
        var urlRequest = URLRequest(url: urlComponents.url!)
        urlRequest.httpMethod = "GET"
        
        let task = urlSession.dataTask(with: urlRequest) { data, response, error in
            if error != nil || data == nil {
                print("Client error!")
                return
            }
            
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error!")
                return
            }
            
            guard let mime = response.mimeType, mime == "application/json" else {
                print("Wrong MIME type!")
                return
            }
            
            do {
                let response = try JSONSerialization.jsonObject(with: data!, options: [])
                let generalData = response as? [ String: Any ]
                var resultsFilterPlant = generalData?["result"] as? [ [String: Any] ] ?? [ [:] ]
                
                for (i, _) in resultsFilterPlant.enumerated() {
                    let topResearchPlantImg = resultsFilterPlant[i]["img"] as? String ?? ""
                    let completeTopResearchPlantImgUrl = self.utils.API_HOST + topResearchPlantImg
                    
                    let topResearchImgUrls = URL(string: completeTopResearchPlantImgUrl)!
                    let topResearchImgData = try? Data(contentsOf: topResearchImgUrls)
                    
                    if let topResearchImg = topResearchImgData {
                        resultsFilterPlant[i]["imgData"] = topResearchImg
                    }
                }
                
                DispatchQueue.main.async() {
                    self.globalResultsFilterPlant = resultsFilterPlant

                    self.filterCollectionView.reloadData()
                }
            } catch {
                print("JSON error: \(error.localizedDescription)")
            }
        }
        
        task.resume()
    }
}

