//
//  NotesViewController.swift
//  Endislopeki
//
//  Created by Viko Wijaya on 26/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit
import CoreData

class NotesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, DrawingViewControllerDelegate{
    
    //    var clickedValue: Int!
    var articleIDPass: String = ""
    var articleName: String = ""
    var articleSubName: String = ""
    private var Request: NSFetchRequest<NSFetchRequestResult>!
    
    var articleArr: [NSManagedObject] = []
    
    let reuseIdentifier         = "NotesCell"
    let tapRecognizer           = UITapGestureRecognizer()
    let tapRecognizerView       = UITapGestureRecognizer()
    var articleNotesImage:        [UIImage] = []
    @IBOutlet weak var collectionViewArticleNotes: UICollectionView!
    
    @IBOutlet weak var titleLabel: UITextField!
    @IBOutlet weak var subTitleLabel: UITextField!
    @IBOutlet weak var notesTextView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var addImage: UIImageView!
    @IBOutlet weak var textViewTappedFrame: UIView!
    
//    var imageToAdd: UIImage = #imageLiteral(resourceName: "Plant1")
    var imagePicker: ImagePicker!
    
    //======================================================================================
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        requestFetchEntityArticle(id: articleIDPass)
        requestFetchEntityImageNotes(id: articleIDPass)
        requestFetchEntityImageDraw(id: articleIDPass)
        
        notesTextView.becomeFirstResponder()
        titleLabel.text     = articleName
        subTitleLabel.text  = articleSubName
        print("ini : \(articleName)")
        print(articleSubName)
        print(articleIDPass)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name:
            UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name:
            UIResponder.keyboardWillHideNotification, object: nil)
        
        let tap: UITapGestureRecognizer         = UITapGestureRecognizer(target: self, action: Selector(("imageTapped")))
        view.addGestureRecognizer(tap)
        let tapTextView: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector(("textViewTapped")))
        view.addGestureRecognizer(tapTextView)
        
        self.addImage.addGestureRecognizer(tap)
        self.textViewTappedFrame.addGestureRecognizer(tapTextView)
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
    }
    
    @IBAction func brushButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "brushSegue", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DrawingViewController {
            destination.delegate = self
        }
    }
    
    @IBAction func addImageTEMPONLY(_ sender: Any) {
        
        //UPDATE ISI NOTES
        self.save(text_notes: notesTextView!)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return articleNotesImage.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! NotesCollectionViewCell
        cell.notesImage.image = articleNotesImage[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    @objc func imageTapped(){
        self.imagePicker.present(from: addImage)
    }
    @objc func textViewTapped(){
        notesTextView.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        //Dispose of any resources that can be recreated.
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 35
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func finishPassing(image: UIImage)
    {
        notesTextView.becomeFirstResponder()
        let attachmentA     = NSTextAttachment()
        attachmentA.image   = image
        let attString       = NSAttributedString(attachment: attachmentA)
        
        //tambah enter
        notesTextView.insertText("\n")
        notesTextView.textStorage.insert(attString, at: notesTextView.selectedRange.location)
        notesTextView.becomeFirstResponder()
        notesTextView.insertText("\n")
        
        requestSaveEntityImageDraw(id: articleIDPass, imageToSave: image, location: notesTextView.selectedRange.location)
    }
    
    //Save ke coredata
    func save(text_notes: UITextView) {
        requestUpdateEntityArticle(index: articleIDPass)
        //Back
    }
    
    func fetchCoreData(entityName: String, limit: Int, filter: [String: Int]) -> [NSManagedObject] {
        let appDelegate =
            UIApplication.shared.delegate as? AppDelegate
        
        // 1
        let managedContext =
            appDelegate?.persistentContainer.viewContext
        
        var object: [NSManagedObject] = []
        
        do {
            Request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            Request.returnsObjectsAsFaults = false
            
            if(filter.count > 0) {
                let filter_key = Array(filter.keys)
                let filter_value = filter[filter_key[0]]!
                let predicate = NSPredicate(format: "\(filter_key[0]) = %@", String(filter_value))
                
                Request.predicate = predicate
            }
            
            if(limit != 0) {
                Request.fetchLimit = limit
            }
            
            let results = try managedContext?.fetch(Request)
            
            object = results as! [NSManagedObject]
        } catch let error as NSError {
            print(error)
        }
        
        return object
    }
    
    func convertImageToBase64String(image : UIImage ) -> String
    {
        let strBase64 =  image.jpegData(compressionQuality: 1)?.base64EncodedString()
        return strBase64!
    }
    
    
    
    //==================================================================================================
    //    func requestSaveEntityArticle()
    //    {
    //        guard let appDelegate =
    //            UIApplication.shared.delegate as? AppDelegate else {return}
    //
    //        let managedContext =
    //            appDelegate.persistentContainer.viewContext
    //
    //        let article                 = Article(context: managedContext)
    //        article.id_article          = Int16(articleIDPass)
    ////        article.title_notes         = //title dari data ko viko
    ////        article.sub_title_notes     = //sub_title dari ko viko
    //        article.text_notes          = ""
    //        articleArr.append(article)
    //    }
    func requestUpdateEntityArticle(index: String)
    {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {return}
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let requestArticle = NSFetchRequest<NSFetchRequestResult>(entityName: "Article")
        requestArticle.returnsObjectsAsFaults = false
        do {
            let result = try managedContext.fetch(requestArticle)
            //            //CARI RESULT KE -> CLICKED VALUE !!!!!!!!!!!!!!!!!
            
            for data in result as! [NSManagedObject] {
                if(data.value(forKey: "id_article") as? String == articleIDPass)
                {
                    let tempData                = data as? Article
                    
                    tempData?.text_notes        = notesTextView.text
                }
            }
            
        } catch {
            print("Failed")
        }
    }
    func requestSaveEntityImageNotes(image: UIImage)
    {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {return}
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let imageNotes          = ImageNotes(context: managedContext)
        imageNotes.id_article   = articleIDPass
        imageNotes.image_notes  = image.pngData()
    }
    func requestSaveEntityImageDraw(id: String, imageToSave: UIImage, location: Int)
    {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {return}
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let imageDrawTextView               = ImageDrawTextView(context: managedContext)
        imageDrawTextView.id_article        = id
        imageDrawTextView.image_draw        = imageToSave.pngData()
        imageDrawTextView.image_location    = Int16(location)
    }
    func requestFetchEntityArticle(id: String)
    {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {return}
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let requestArticle = NSFetchRequest<NSFetchRequestResult>(entityName: "Article")
        requestArticle.returnsObjectsAsFaults = false
        do {
            let result = try managedContext.fetch(requestArticle)
            
            for data in result as! [NSManagedObject] {
                if(data.value(forKey: "id_article") as? String == id)
                {
                    if(data.value(forKey: "text_notes") != nil)
                    {
                        notesTextView.text = data.value(forKey: "text_notes") as? String
                    }
                }
            }
            
        } catch {
            
            print("Failed")
        }
    }
    func requestFetchEntityImageNotes(id: String)
    {
        //        var index: Int
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {return}
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let requestImageNote = NSFetchRequest<NSFetchRequestResult>(entityName: "ImageNotes")
        requestImageNote.returnsObjectsAsFaults = false
        do {
            let result = try managedContext.fetch(requestImageNote)
            articleNotesImage.removeAll()
            for data in result as! [NSManagedObject] {
                if(data.value(forKey: "id_article") as? String == id)
                {
                    if(data.value(forKey: "image_notes") != nil)
                    {
                        guard let tempImage = UIImage(data: data.value(forKey: "image_notes") as! Data) else { return }
                        articleNotesImage.append(tempImage)
                    }
                }
            }
            
        } catch {
            print("Failed")
        }
    }
    func requestFetchEntityImageDraw(id: String)
    {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {return}
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let requestImageNote = NSFetchRequest<NSFetchRequestResult>(entityName: "ImageDrawTextView")
        requestImageNote.returnsObjectsAsFaults = false
        do {
            let result = try managedContext.fetch(requestImageNote)
            for data in result as! [NSManagedObject] {
                if(data.value(forKey: "id_article") as? String != nil)
                {
                    if(data.value(forKey: "id_article") as? String == id){
                        
                        print("MASUK1")
                        if(data.value(forKey: "image_draw") != nil)
                        {
                            print("MASUK")
                            guard let tempImage = UIImage(data: data.value(forKey: "image_draw") as! Data) else { return }
                            //
                            let attachmentA     = NSTextAttachment()
                            attachmentA.image   = tempImage
                            let attString       = NSAttributedString(attachment: attachmentA)
                            
                            //tambah enter
                            notesTextView.insertText("\n")
                            notesTextView.textStorage.insert(attString, at: ((data.value(forKey: "image_location") as? Int)!))
                        }
                    }
                }
            }
            
        } catch {
            print("Failed")
        }
        requestUpdateEntityArticle(index: id)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension NotesViewController: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        requestSaveEntityImageNotes(image: image!)
        articleNotesImage.append(image!)
        self.collectionViewArticleNotes.reloadData()
    }
}
