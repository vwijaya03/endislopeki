//
//  MenuTabBarController.swift
//  Endislopeki
//
//  Created by Viko Wijaya on 25/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class MenuTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.tabBar.tintColor = #colorLiteral(red: 0.3411764706, green: 0.6588235294, blue: 0.1098039216, alpha: 1)
        self.tabBar.unselectedItemTintColor = #colorLiteral(red: 0.6470588235, green: 0.8392156863, blue: 0.6549019608, alpha: 1)
        self.selectedIndex = 1
    }
}
