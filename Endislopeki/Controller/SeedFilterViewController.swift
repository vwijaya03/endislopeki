//
//  SeedFilterViewController.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 23/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class SeedFilterViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    var morphologyTypes = ["Bulat Telur", "Bulat", "Ginjal", "Memanjang"]
    var morphologyImages = [#imageLiteral(resourceName: "Biji Bulat Telur"), #imageLiteral(resourceName: "Biji Bulat"), #imageLiteral(resourceName: "Biji Ginjal"), #imageLiteral(resourceName: "Biji Ginjal")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let seedFilterCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "seedFilterCollectionCellReuseIdentifier", for:indexPath) as! SeedFilterCollectionViewCell
        
        seedFilterCollectionViewCell.filterImage.image = morphologyImages[indexPath.row]
        seedFilterCollectionViewCell.filterLabel.text = morphologyTypes[indexPath.row]
        
        return seedFilterCollectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let filterStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FilterStoryBoard") as! FilterViewController
        let indexPathNumber = morphologyTypes[indexPath.row]
        print(indexPathNumber)
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
