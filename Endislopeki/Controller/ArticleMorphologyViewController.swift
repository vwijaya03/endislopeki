//
//  ArticleMorphologyViewController.swift
//  FiturNotes
//
//  Created by Kevin Josal on 19/08/19.
//  Copyright © 2019 Kevin Josal. All rights reserved.
//

import UIKit

class ArticleMorphologyViewController: UIViewController {

    @IBOutlet weak var sistemPerakaran: UILabel!
    @IBOutlet weak var jenisBatang: UILabel!
    @IBOutlet weak var percabanganBatang: UILabel!
    @IBOutlet weak var bentukBatang: UILabel!
    @IBOutlet weak var tipeDaun: UILabel!
    @IBOutlet weak var susunanTulangDaun: UILabel!
    @IBOutlet weak var tipeBunga: UILabel!
    @IBOutlet weak var bentukBuah: UILabel!
    @IBOutlet weak var bentukBiji: UILabel!
    
    @IBOutlet weak var sistemPerakaranValue: UILabel!
    @IBOutlet weak var jenisBatangValue: UILabel!
    @IBOutlet weak var percabanganBatangValue: UILabel!
    @IBOutlet weak var bentukBatangValue: UILabel!
    @IBOutlet weak var tipeDaunValue: UILabel!
    @IBOutlet weak var susunanTulangDaunValue: UILabel!
    @IBOutlet weak var tipeBungaValue: UILabel!
    @IBOutlet weak var bentukBuahValue: UILabel!
    @IBOutlet weak var bentukBijiValue: UILabel!
    
    @IBOutlet weak var textLabel: UILabel!
    
    var dataMorphologies: [ [String : Any] ] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        textLabel.numberOfLines = 2
        textLabel.text = "ADADASDASDADASDASDASDASDASDASDASDASDASDASDASDASDASDASDASDASDASDASASadadasddjdjjdjdjdjdjdjdjdjdjdjjjjdjdjdjdjdjdjdjdjddjdjdjdjdjjdjjdjjjcjcjcjcjcjcDASAsda"
        
        sistemPerakaran.text = dataMorphologies[0]["morfologyName"] as? String ?? ""
        sistemPerakaranValue.text = dataMorphologies[0]["morfology"] as? String ?? ""
        
        jenisBatang.text = dataMorphologies[1]["morfologyName"] as? String ?? ""
        jenisBatangValue.text = dataMorphologies[1]["morfology"] as? String ?? ""
        
        percabanganBatang.text = dataMorphologies[2]["morfologyName"] as? String ?? ""
        percabanganBatangValue.text = dataMorphologies[2]["morfology"] as? String ?? ""
        
        bentukBatang.text = dataMorphologies[3]["morfologyName"] as? String ?? ""
        bentukBatangValue.text = dataMorphologies[3]["morfology"] as? String ?? ""
        
        tipeDaun.text = dataMorphologies[4]["morfologyName"] as? String ?? ""
        tipeDaunValue.text = dataMorphologies[4]["morfology"] as? String ?? ""
        
        susunanTulangDaun.text = dataMorphologies[5]["morfologyName"] as? String ?? ""
        susunanTulangDaunValue.text = dataMorphologies[5]["morfology"] as? String ?? ""
        
        tipeBunga.text = dataMorphologies[6]["morfologyName"] as? String ?? ""
        tipeBungaValue.text = dataMorphologies[6]["morfology"] as? String ?? ""
        
        bentukBuah.text = dataMorphologies[7]["morfologyName"] as? String ?? ""
        bentukBuahValue.text = dataMorphologies[7]["morfology"] as? String ?? ""
        
        bentukBiji.text = dataMorphologies[8]["morfologyName"] as? String ?? ""
        bentukBijiValue.text = dataMorphologies[8]["morfology"] as? String ?? ""
    }
}
