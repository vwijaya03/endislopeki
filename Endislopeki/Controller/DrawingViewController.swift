//
//  DrawingViewController.swift
//  Endislopeki
//
//  Created by Viko Wijaya on 26/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

protocol DrawingViewControllerDelegate {
    func finishPassing(image: UIImage)
}

class DrawingViewController: UIViewController {
    
    var delegate: DrawingViewControllerDelegate?
    private var lastPoint       = CGPoint.zero
    private var color           = UIColor.black
    private var brushWidth      : CGFloat = 10.0
    private var opacity         : CGFloat = 1.0
    private var swiped          = false
    private var currentBrush    = "brush"
    private var pencilPositionY : CGFloat = 0.0
    private var brushPositionY  : CGFloat = 0.0
    private var eraserPositionY : CGFloat = 0.0
    private var tempPositionY   : CGFloat = 837.0
    @IBOutlet weak var imageCanvasMain: UIImageView!
    @IBOutlet weak var imageCanvasTemp: UIImageView!
    @IBOutlet weak var buttonPencil: UIButton!
    @IBOutlet weak var buttonBrush: UIButton!
    @IBOutlet weak var buttonEraser: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.tintColor = .white
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        swiped = false
        lastPoint = touch.location(in: view)
    }
    
    func drawLine(from fromPoint: CGPoint, to toPoint: CGPoint) {
        //1
        UIGraphicsBeginImageContext(view.frame.size)
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        imageCanvasTemp.image?.draw(in: view.bounds)
        
        //2
        context.move(to: fromPoint)
        context.addLine(to: toPoint)
        
        //3
        
        if(currentBrush == "brush")
        {
            brushWidth          = 10.0
            color               = UIColor.black
        }
        else if(currentBrush == "pencil")
        {
            brushWidth          = 3.0
            color               = UIColor.gray
        }
        else if(currentBrush == "eraser")
        {
            color               = UIColor.white
        }
        
        context.setLineCap(.round)
        context.setBlendMode(.normal)
        context.setLineWidth(brushWidth)
        context.setStrokeColor(color.cgColor)
        
        //4
        context.strokePath()
        
        //5
        imageCanvasTemp.image = UIGraphicsGetImageFromCurrentImageContext()
        imageCanvasTemp.alpha = opacity
        UIGraphicsEndImageContext()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        //6
        swiped = true
        let currentPoint = touch.location(in: view)
        drawLine(from: lastPoint, to: currentPoint)
        
        //7
        lastPoint = currentPoint
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !swiped {
            //draw a single point
            drawLine(from: lastPoint, to: lastPoint)
        }
        
        //Merge Temp and Main
        UIGraphicsBeginImageContext(imageCanvasMain.frame.size)
        
        imageCanvasMain.image?.draw(in: view.bounds, blendMode: .normal, alpha: 1.0)
        imageCanvasTemp?.image?.draw(in: view.bounds, blendMode: .normal, alpha: opacity)
        imageCanvasMain.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        imageCanvasTemp.image = nil
    }
    
    private func resetPositionY()
    {
        pencilPositionY = tempPositionY
        brushPositionY  = tempPositionY
        eraserPositionY = tempPositionY
    }
    
    private func rePosition()
    {
        buttonPencil.frame      = CGRect(x: buttonPencil.frame.origin.x, y: pencilPositionY, width: buttonPencil.frame.width, height: buttonPencil.frame.height)
        buttonBrush.frame       = CGRect(x: buttonBrush.frame.origin.x, y: brushPositionY, width: buttonBrush.frame.width, height: buttonBrush.frame.height)
        buttonEraser.frame      = CGRect(x: buttonEraser.frame.origin.x, y: eraserPositionY, width: buttonEraser.frame.width, height: buttonEraser.frame.height)
    }
    
    @IBAction func pencilButtonPressed(_ sender: Any) {
        currentBrush = "pencil"
        
        resetPositionY()
        pencilPositionY     = tempPositionY - 20.0
        rePosition()
    }
    
    @IBAction func brushButtonPressed(_ sender: Any) {
        currentBrush = "brush"
        
        resetPositionY()
        brushPositionY     = tempPositionY - 20.0
        rePosition()
    }
    @IBAction func eraserButtonPressed(_ sender: Any) {
        currentBrush = "eraser"
        
        resetPositionY()
        eraserPositionY     = tempPositionY - 20.0
        rePosition()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        delegate?.finishPassing(image: imageCanvasMain.image!)
        navigationController?.popToRootViewController(animated: true)
    }
}
