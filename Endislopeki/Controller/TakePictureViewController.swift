//
//  TakePictureViewController.swift
//  Endislopeki
//
//  Created by Viko Wijaya on 31/07/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit
import AVFoundation
import CoreML
import Vision
import ImageIO

class TakePictureViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, AVCapturePhotoCaptureDelegate {
    @IBOutlet weak var bgActivityIndicatorView: UIView!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var processImageLabel: UILabel!
    @IBOutlet weak var processImageActivityIndicator: UIActivityIndicatorView!
    
    let imagePicker = UIImagePickerController()
    
    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var tapped: Bool = false
    
    let utils = Utils.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        showLoading(bool: false)
    }
    
    func showLoading(bool: Bool) {
        processImageActivityIndicator.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        processImageActivityIndicator.isHidden = !bool
        processImageLabel.isHidden = !bool
        bgActivityIndicatorView.isHidden = !bool
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .high
        
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            else {
                print("Unable to access back camera!")
                return
        }
        
        if(backCamera.isFocusModeSupported(.continuousAutoFocus)) {
            do {
                try backCamera.lockForConfiguration()
                backCamera.focusMode = .continuousAutoFocus
                backCamera.unlockForConfiguration()
            } catch let error {
                print(error)
            }
        }
                
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            //Step 9
            stillImageOutput = AVCapturePhotoOutput()
            
            if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                captureSession.addInput(input)
                captureSession.addOutput(stillImageOutput)
                setupLivePreview()
            }
        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.captureSession != nil {
            self.captureSession.stopRunning()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setupLivePreview() {
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait
        
        previewView.layer.addSublayer(videoPreviewLayer)
        
        //Step12
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
            self.captureSession.startRunning()
            //Step 13
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.previewView.frame
                self.previewView.bringSubviewToFront(self.cameraButton)
                self.previewView.bringSubviewToFront(self.flashButton)
                self.previewView.bringSubviewToFront(self.bgActivityIndicatorView)
                self.previewView.bringSubviewToFront(self.processImageActivityIndicator)
            }
        }
    }
    
    @IBAction func flashLight(_ sender: UIButton) {
        if(tapped == false) {
            tapped = true
            flashButton.setBackgroundImage(UIImage(named: "flashEnable"), for: .normal)
        } else {
            tapped = false
            flashButton.setBackgroundImage(UIImage(named: "flashDisable"), for: .normal)
        }
        
        guard let device = AVCaptureDevice.default(for: AVMediaType.video)
            else {return}
        
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                
                if(tapped == true) {
                    device.torchMode = .on
                } else {
                    device.torchMode = .off
                }
                
                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
    }
    
    @IBAction func didTakePhoto(_ sender: Any) {
//        let rootViewController = storyboard.instantiateViewController(withIdentifier: "TakePictureViewController") as! TakePictureViewController
        //let objNavi = UINavigationController(rootViewController: self)
//
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let resultVC = storyboard.instantiateViewController(withIdentifier: "ResultViewController") as! ResultViewController
//
//        objNavi.pushViewController(resultVC, animated: true)
        
        //let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //let newViewController = storyBoard.instantiateViewController(withIdentifier: "ResultViewController") as! ResultViewController
//        self.present(newViewController, animated: true, completion: nil)
        
        //self.navigationController?.pushViewController(newViewController, animated: true)
        
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        stillImageOutput.capturePhoto(with: settings, delegate: self)
    }
    
    // MARK: photoOutput default function untuk menerima capture photo dari stillImageOutput.capturePhoto
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
        guard let image = UIImage(data: imageData) else
        {
            print("image nil")
            return
        }
        
        updateClassifications(for: image)
    }
    
    /// - Tag: PerformRequests
    func updateClassifications(for image: UIImage) {
        //classificationLabel.text = "Classifying..."
        
        let orientation = CGImagePropertyOrientation(rawValue: UInt32(image.imageOrientation.rawValue))
        guard let ciImage = CIImage(image: image) else { fatalError("Unable to create \(CIImage.self) from \(image).") }
        
        DispatchQueue.global(qos: .userInitiated).async {
            let handler = VNImageRequestHandler(ciImage: ciImage, orientation: orientation!)
            do {
                try handler.perform([self.classificationRequest])
            } catch {
                /*
                 This handler catches general image processing errors. The `classificationRequest`'s
                 completion handler `processClassifications(_:error:)` catches errors specific
                 to processing that request.
                 */
                print("Failed to perform classification.\n\(error.localizedDescription)")
            }
        }
    }
    
    /// - Tag: MLModelSetup
    lazy var classificationRequest: VNCoreMLRequest = {
        do {
            /*
             Use the Swift class `MobileNet` Core ML generates from the model.
             To use a different Core ML classifier model, add it to the project
             and replace `MobileNet` with that model's generated Swift class.
             */
            let model = try VNCoreMLModel(for: EdibleWildPlantsML().model)
            
            let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
                self?.processClassifications(for: request, error: error)
            })
            request.imageCropAndScaleOption = .centerCrop
            return request
        } catch {
            fatalError("Failed to load Vision ML model: \(error)")
        }
    }()
    
    /// Updates the UI with the results of the classification.
    /// - Tag: ProcessClassifications
    func processClassifications(for request: VNRequest, error: Error?) {
        DispatchQueue.main.async {
            guard let results = request.results else {
                //self.classificationLabel.text = "Unable to classify image.\n\(error!.localizedDescription)"
                print("Unable to classify image.\n\(error!.localizedDescription)")
                return
            }
            // The `results` will always be `VNClassificationObservation`s, as specified by the Core ML model in this project.
            let classifications = results as! [VNClassificationObservation]
            
            if classifications.isEmpty {
                //self.classificationLabel.text = "Nothing recognized."
                print("Nothing recognized.")
            } else {
                // Display top classifications ranked by confidence in the UI.
                let topClassifications = classifications.prefix(7)
                let descriptions = topClassifications.map { classification in
                    // Formats the classification for display; e.g. "(0.37) cliff, drop, drop-off".
                    //return String(format: "  (%.2f) %@", classification.confidence, classification.identifier)
                    return String(format: "%@", classification.identifier)
                }
                
                print(descriptions)
                //print(descriptions[0])
                //self.classificationLabel.text = "Classification:\n" + descriptions.joined(separator: "\n")
                //print("Classification:\n" + descriptions.joined(separator: "\n"))
                
                self.showLoading(bool: true)
                
                let urlSession = URLSession.shared
                guard let url = URL(string: self.utils.API_SEARCH_PLANT) else {
                    return
                }
                
                guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
                    return
                }
                var items = [URLQueryItem]()
                var otherResults: String = ""
                
                for (index, value) in descriptions.enumerated() {
                    if(index > 0) {
                        if(index == (descriptions.count - 1)) {
                            otherResults = otherResults + value
                        } else {
                            otherResults = otherResults + value + ", "
                        }
                        
                    }
                }
                
                items.append(URLQueryItem(name: "result", value: descriptions[0]))
                items.append(URLQueryItem(name: "otherResults", value: otherResults))
                urlComponents.queryItems = items
                
                var urlRequest = URLRequest(url: urlComponents.url!)
                urlRequest.httpMethod = "GET"
                
                let task = urlSession.dataTask(with: urlRequest) { data, response, error in
                    if error != nil || data == nil {
                        print("Client error!")
                        return
                    }
                    
                    guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                        print("Server error!")
                        return
                    }
                    
                    guard let mime = response.mimeType, mime == "application/json" else {
                        print("Wrong MIME type!")
                        return
                    }
                    
                    do {
                        let response = try JSONSerialization.jsonObject(with: data!, options: [])
                        let generalData = response as? [ String: Any ]
                        let result = generalData?["result"] as? [String: Any] ?? ["result": ""]
                        let otherResults = generalData?["otherResults"] as? [ [String: Any] ] ?? [ ["otherResults": [] ] ]
                        
                        DispatchQueue.main.async() {
                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ResultViewController") as! ResultViewController
                            newViewController.result = result
                            newViewController.otherResults = otherResults
                            
                            self.showLoading(bool: false)
                            self.navigationController?.pushViewController(newViewController, animated: true)
                        }
                        
                    } catch {
                        print("JSON error: \(error.localizedDescription)")
                    }
                }
                
                task.resume()
            }
        }
    }
}
