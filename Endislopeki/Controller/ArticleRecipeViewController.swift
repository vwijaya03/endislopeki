//
//  ArticleRecipeViewController.swift
//  FiturNotes
//
//  Created by Kevin Josal on 19/08/19.
//  Copyright © 2019 Kevin Josal. All rights reserved.
//

import UIKit

class ArticleRecipeViewController: UIViewController {

    @IBOutlet weak var recipeTextView: UITextView!
    
    var recipeDescription: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        recipeTextView.text = recipeDescription
    }
}
