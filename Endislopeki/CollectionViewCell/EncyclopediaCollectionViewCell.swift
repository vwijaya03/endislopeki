//
//  EncyclopediaCollectionViewCell.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 26/07/19.
//  Copyright © 2019 Guildy Harvey. All rights reserved.
//

import UIKit

class EncyclopediaCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var encyclopediaImage: UIImageView!
    @IBOutlet weak var labelNama: UILabel!
    @IBOutlet weak var labelKeterangan: UILabel!
    
    
}
