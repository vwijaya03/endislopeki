//
//  FilterButtonCollectionViewCell.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 22/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class FilterButtonCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageButton: UIImageView!
    
}
