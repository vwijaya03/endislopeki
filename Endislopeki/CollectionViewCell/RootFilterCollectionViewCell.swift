//
//  RootFilterCollectionViewCell.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 26/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class RootFilterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var filterImage: UIImageView!
    @IBOutlet weak var filterLabel: UILabel!
    
}
