//
//  SavedArticleCollectionViewCell.swift
//  FiturNotes
//
//  Created by Kevin Josal on 12/08/19.
//  Copyright © 2019 Kevin Josal. All rights reserved.
//

import UIKit

class SavedArticleCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var savedArticleImage: UIImageView!
    @IBOutlet weak var savedArticleTitle: UILabel!
    @IBOutlet weak var savedArticleDescription: UILabel!
}
