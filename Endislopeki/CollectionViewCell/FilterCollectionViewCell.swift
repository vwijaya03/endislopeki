//
//  FilterCollectionViewCell.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 12/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class FilterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var filterImage: UIImageView!
    @IBOutlet weak var labelNama: UILabel!
    @IBOutlet weak var labelKeterangan: UILabel!
}
