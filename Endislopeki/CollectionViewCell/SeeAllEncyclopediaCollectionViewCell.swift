//
//  SeeAllEncyclopediaCollectionViewCell.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 08/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class SeeAllEncyclopediaCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var encyclopediaImage: UIImageView!
    @IBOutlet weak var labelNama: UILabel!
    @IBOutlet weak var labelKeterangan: UILabel!
}
