//
//  TopResearchCollectionViewCell.swift
//  Endislopeki
//
//  Created by Guildy Harvey on 26/07/19.
//  Copyright © 2019 Guildy Harvey. All rights reserved.
//

import UIKit

class TopResearchCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var topResearchImage: UIImageView!
    @IBOutlet weak var topResearchName: UILabel!
    @IBOutlet weak var topResearchDescription: UILabel!
    @IBOutlet weak var topResearchDarkBg: UIView!
    
}
