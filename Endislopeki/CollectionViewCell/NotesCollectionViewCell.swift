//
//  NotesCollectionViewCell.swift
//  Endislopeki
//
//  Created by Viko Wijaya on 26/08/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class NotesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var notesImage: UIImageView!
}
